import React from "react";
import { Container, Col, Row } from "react-bootstrap";

export default function Learn() {
  return (
    <div>
      <h2 className="text-center mt-3 text-decoration-underline">
        Our Vision Page
      </h2>
      <Container className="my-5">
        <Row className="my-5">
          <Col
            className="me-3 p-2 w-100"
            col={6}
            style={{ backgroundColor: "DarkSlateGrey", color: "white" }}
          >
            <h3 className="text-center">Vision</h3>
            <ul>
              <li>
                To be the best SW Professional Training Center in Cambodia
              </li>
            </ul>
          </Col>
          <Col
            className="ms-3 p-2 w-100"
            col={6}
            style={{ backgroundColor: "DarkSlateGrey", color: "white" }}
          >
            <h3 className="text-center">Mission</h3>
            <ul>
              <li>High quality training and research</li>
              <li>
                Developing Capacity of SW Experts to be Leaders in IT Fiele
              </li>
              <li>Developing sustainable ICT Program</li>
            </ul>
          </Col>
        </Row>

        <Row className="my-5">
          <Col
            className="me-3 p-2 w-100"
            col={6}
            style={{ backgroundColor: "DarkSlateGrey", color: "white" }}
          >
            <h3 className="text-center">Strategy</h3>
            <ul>
              <li>
                Best training method with up to date curriculum and environment
              </li>
              <li>
                Cooperation with the best 17 industry to guarantee student's
                career and benifit
              </li>
              <li>Additional Soft Skk Management Leadership training</li>
            </ul>
          </Col>
          <Col
            className="ms-3 p-2 w-100"
            col={6}
            style={{ backgroundColor: "DarkSlateGrey", color: "white" }}
          >
            <h3 className="text-center">Slogan</h3>
            <ul>
              <li>"KSHRD connects you to various opportunities in IT Field</li>
              <li>
                Raising brand awareness with continuous advertisement of SNS and
                any other media
              </li>
            </ul>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
