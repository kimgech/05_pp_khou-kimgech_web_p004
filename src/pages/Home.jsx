import React from "react";
import { Card, Button, Container, Row, Col } from "react-bootstrap";

export default function Home({ data, setData }) {
  return (
    <div>
      <Container className="mt-3 container-fluid w-75">
        <h2 className="text-dark text-decoration-underline">Trending Course</h2>
      </Container>
      <Container className="my-5 mt-0">
        <Row>
          <Col>
            {data.map((item, idx) => {
              return (
                <Card
                  style={{ width: "15rem" }}
                  className="d-inline-block mx-3 col-3 text-center"
                >
                  <Card.Img variant="top" src={item.thumbnail} />
                  <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text>{item.description}</Card.Text>
                    <Button variant="primary">{item.price} </Button>
                  </Card.Body>
                </Card>
              );
            })}
          </Col>
        </Row>
      </Container>
    </div>
  );
}
