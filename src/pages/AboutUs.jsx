import React from "react";
import { Container, Col, Row } from "react-bootstrap";

export default function AboutUs() {
  return (
    <div>
      <h2 className="text-center mt-3 text-decoration-underline">
        About Us Page
      </h2>
      <Container className="mt-3 my-5 text-center">
        <Row className="my-5">
          <Col col={6}>
            {" "}
            <img
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
              class="img-fluid"
              alt="..."
            ></img>
          </Col>
          <Col
            col={6}
            style={{ backgroundColor: "DarkSlateGrey", color: "white" }}
          >
            <p className="mt-3">
              {" "}
              Koma Software HRD Canter is an academy training center for
              training software professionals in cooperations with Kones
              International Cooperation Agency (CICA) and Webcash in April, 2013
              in Phnom Penh, Cambodia
            </p>{" "}
            <p>
              From 2020, Korea Software HRD Center has been become Global NGO
              with the name Foundation for Korea Software Global Aid (SOA). mein
              sponsored by Webcash Group, to continue mission for ICT
              Development in Cambodia and wil recruit60 1080 scholarship
              students every year.
            </p>
          </Col>
        </Row>

        <Row className="my-5">
          <Col
            col={6}
            style={{ backgroundColor: "DarkSlateGrey", color: "white" }}
          >
            <p className="mt-3">
              {" "}
              Koma Software HRD Canter is an academy training center for
              training software professionals in cooperations with Kones
              International Cooperation Agency (CICA) and Webcash in April, 2013
              in Phnom Penh, Cambodia
            </p>{" "}
            <p>
              From 2020, Korea Software HRD Center has been become Global NGO
              with the name Foundation for Korea Software Global Aid (SOA). mein
              sponsored by Webcash Group, to continue mission for ICT
              Development in Cambodia and wil recruit60 1080 scholarship
              students every year.
            </p>
          </Col>
          <Col col={6}>
            {" "}
            <img
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
              class="img-fluid"
              alt="kshrd"
            ></img>
          </Col>
        </Row>
        <Row className="my-5">
          <Col col={6}>
            {" "}
            <img
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
              class="img-fluid"
              alt="kshrd"
            ></img>{" "}
          </Col>
          <Col
            col={6}
            style={{ backgroundColor: "DarkSlateGrey", color: "white" }}
          >
            <p className="mt-3">
              {" "}
              Koma Software HRD Canter is an academy training center for
              training software professionals in cooperations with Kones
              International Cooperation Agency (CICA) and Webcash in April, 2013
              in Phnom Penh, Cambodia
            </p>{" "}
            <p>
              From 2020, Korea Software HRD Center has been become Global NGO
              with the name Foundation for Korea Software Global Aid (SOA). mein
              sponsored by Webcash Group, to continue mission for ICT
              Development in Cambodia and wil recruit60 1080 scholarship
              students every year.
            </p>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
