import React from 'react'
import {Navbar, Container, Nav} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function NavMenu() {
  return (

    <Navbar bg="dark" expand="lg" variant="dark">
    <Container>
    <Navbar.Brand href="#home">KG PT-004</Navbar.Brand>
    <Nav className="justify-content-end">
      <Nav.Link as={Link} to='/'>Home</Nav.Link>
      <Nav.Link as={Link} to='/aboutus'>About Us</Nav.Link>
      <Nav.Link as={Link} to='/vision'>Our Vision</Nav.Link>
      <Nav.Link as={Link} to='/more'>More</Nav.Link>
    </Nav>
    </Container>
  </Navbar>
  
  )
}
