import React from 'react'

export default function MyFooter() {
  return (
    <div>
    <section fixed="bottom" className="myfooter">
    <footer class="bg-secondary text-white text-center">
      <div class="container">
        <div class="row p-3">
          <div class="col-lg-4 justify-content-center ">
          <img src="https://kshrd.com.kh/static/media/logo.f368c431.png" class="img" alt="logo" style={{width:'90px',height:'110px'}}></img>
            <p class="text-uppercase mt-3">Korea Software HRD Center</p>
          </div>
          <div class="col-lg-4 col-md-5 justify-content-center">
            <h4 class="text-uppercase">Address</h4>
            <p className='mt-5'>Address: #12, St 323, Sangkat Boeung Kak II, Khan Toul Kork, Phnom Penh, Cambodia.</p>
          </div>
          <div className="col-lg-4 col-md-5 ">
            <h4 class="text-uppercase mb-0">Contact</h4>
           <div className='mt-3'>
           <p>Tel: 012 998 919 (Khmer)</p>
           <p>Tel: 085 402 605 (Korean)</p>
           <p>Email: info.kshrd@gmail pirum.gm@gmail.com </p></div>
          </div>    
        </div>
      </div>
      <div className='text-center'>
        © 2022 Copyright : Khou Kimgech 
      </div>
    </footer>
  </section>
    </div>
  )
}
